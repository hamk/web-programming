<?php

# === STRING ===

# 1. Write PhP script with two string variables. Assign any text to these variables. Join them together.
# Print the length of the string. (Hint: string function)
$word1 = "Hello ";
$word2 = "World!";
$joined = $word1 . $word2;
echo "The string length is " . strlen($joined) . ".<br/>";

# 2. In your above code, Change the double quotes to single quotes.Then run the script again.
# Did it have any effect?
$word1_sq = 'Hello ';
$word2_sq = 'World!';
$joined_sq = $word1 . $word2;
echo 'The string length is ' . strlen($joined_sq) . '.<br/>';
// No, there was no difference. But what you probably want to hear that single quotes take the string literally, so we
// don't need to escape any characters besides single quotes. We can't use string templates in strings defined using
// single quotes. In double quotes, there are more characters that we have to escape, for example the dollar sign as
// well as the double quote character.

# 3. Put a single quote at the beginning of your text and double quote at the end.
# What happens now when you run your code?
//echo 'The string length is " . strlen($joined) . '.<br/>";
// I can't keep this code uncommented because it errors out the interpreter. The interpreter can't find
// the end of the first single quote and errors.

# 4. Delete the dollar sign from the variable name. Run your code. What error did you get?
# Write the error message.
//variable = "my variable";
# Error: Can't use temporary expression in write context. Undefined constant 'variable'

# 5. Put the dollar sign back and remove one of the semicolon from the code. Run your code again.
#What error did you get this time?
//$variable = "my variable"
# Error: Expected: semicolon

# 6. Write a PHP script to get the following display
echo "\" It is Markku's car.\"<br/>";
echo "Random characters: del c:\*.* \"<br/>";


# === OPERATORS ===
# 1. Write a script to add up the numbers: 298, 234, 46. Use echo statement to output your answer.
$sum = 298 + 234 + 46;
echo "The sum is $sum.<br/>";

# 2. Use variables to calculate the following : (87 + 44) + (200 * 15) / 10
$eighty_seven = 87;
$forty_four = 44;
$two_hundred = 200;
$fifteen = 15;
$ten = 10;
$summation = $eighty_seven + $forty_four;
$multiplication = $two_hundred * $fifteen;
$expression = $summation + $multiplication / $ten;
echo "The result of the expression is $expression.<br/>";