<!--
PHP Exercises repo: https://gitlab.com/hamk/web-programming
Static website repo: https://gitlab.com/hamk/gamefence
GitLab Pages: https://hamk.gitlab.io/gamefence
-->

<!--1. Write a script that gets the current month and prints one of the following responses, depending on whether it's
August or not:-->
<?php
// it's very bad to use date("F") in order to check if it's August because different locales will have a different
// name for this month (like different languages). So we ALWAYS compare the number of the month. Plus, it's more
// efficient to compare two numbers than comparing two strings char by char.
if (date('m') == 8)
	echo "It's August, so it's still holiday.<br/><br/>";
else {
	$month_name = date("F");
	echo "Not August, This is Month - $month_name, so I don't have any holidays.<br/><br/>";
}
?>


<!--2. Assign color red to a variable name $color and check to print one the following responses using if else statement-->
<?php
$color = "blue";

if ($color == "#ff0000" || $color == "#f00" || $color == "red" || $color == "#ff0000ff" || $color == "#f00f")
	echo "The color is <span style='color: $color'>red</span>.<br/><br/>";
else
	echo "The color is <span style='color: $color'>not red</span>.<br/><br/>";
?>


<!--3. Write a program to grade students based on their total score for a subject. The grading scheme is:
Excellent : >80 ;Great >70 & less than 80;Good >60 & less than 70; Pass >50 & less than 60 & Fail <50-->
<?php
$grade = "Enter a percentage!";

if (isset($_POST["percentage"]))
	if ($_POST["percentage"] > 80) $grade = "Excellent";
	else if ($_POST["percentage"] > 70) $grade = "Great";
	else if ($_POST["percentage"] > 60) $grade = "Good";
	else if ($_POST["percentage"] > 50) $grade = "Pass";
	else $grade = "Fail";
?>

<form action="exercise3.php" method="post">
    <label title="Percentage">
        <input type="number"
               name="percentage"
               value="<?php if (isset($_POST["percentage"])) echo $_POST["percentage"] ?>"
               min="0"
               max="100"
               placeholder="Enter Percentage"
               required>
    </label>
    <input type="submit" value="Check">
</form>

<?php
echo "Grade: $grade<br/><br/>";
?>

<!--4. Write a program to get inputs (age and name) from the user and based on their age, decide if he/she is eligible
for voting. (18 or more than 18 years is eligible for voting.)-->
<?php
$eligible = "Unknown";
if (isset($_POST["age"])) {
	if ($_POST["age"] >= 18)
		$eligible = "Eligible";
	else
		$eligible = "Not eligible";
}
?>


<form action="exercise3.php" method="post">
    <label title="Name">
        <input
                name="name"
                placeholder="Enter name"
                type="text"
                value="<?php if (isset($_POST["name"])) echo $_POST["name"] ?>"
                required>
    </label>
    &nbsp;
    <label title="Age">
        <!-- https://en.wikipedia.org/wiki/List_of_the_verified_oldest_people -->
        <input type="number"
               name="age"
               placeholder="Enter age"
               value="<?php if (isset($_POST["age"])) echo $_POST["age"] ?>"
               min="0"
               max="119"
               required>
    </label>
    &nbsp;
    <input type="submit" value="Check eligibility">
</form>

<?php
// went full hardcore more on this one lol
function form_possessive(string $name): string {
    // if string ends with an 's', we have to append a single quote char at the end
    if (str_ends_with($name, "s")) {
        return $name . "'";
    } else {
        return $name . "'s";
    }
}
?>

<span><?php if (isset($_POST["name"])) echo form_possessive($_POST["name"]); else echo "Person's" ?> eligibility for voting: </span>
<span style="font-size: 1.5rem; font-weight: bold">
    <?php echo $eligible ?>.<br/><br/>
</span>

<!--5. Use a looping statement to construct the following pattern:
12345678
1234567
123456
12345
1234
123
12
1
-->
<?php
for ($i = 8; $i >= 1; $i--) {
    for ($j = 1; $j <= $i; $j++) {
        echo $j;
    }
    echo "<br/>";
}
?>
<br/>


<!--6. Use While loop to print the following pattern:

*
**
***
****
*****
******
*******
********
-->
<?php
for ($i = 1; $i <= 8; $i++) {
	echo str_repeat("*", $i) . "<br/>";
}
?>
<br/>

<!--7. Create a GitHub repo and enable GitHub pages for the repo. Upload your HTML files (your website that you did
with Tommi)  to the repo. Include the link to the repo and your web page in the php file. -->