<?php

# 1. Write a PHP script to get the PHP configuration information. hint phpinfo()
//echo phpinfo();

# 2. Write a simple PHP script to print your information (Name and your groupid).
echo "My name is Martin and my groupid is BBCAP2021.<br/>";

# 3. Write PHP code to display the following message
echo "Hello world ! My name is \"David\"<br/>";

# 4. Write PHP code to display the following paragraph.
echo "It is possible to place variables inside of double-quoted strings (e.g. \"string here and a \$variable\").
<b>By putting a variable</b> inside the quotes (\" \") you are telling PHP that you want it to grab the string value of
that variable and use it in the string. The example below shows an example of this feature (it does not because
we are instructed to use constants and not variables).<br/>";

# 5. Use two constants to assign your first name and last name. Print to get the following output.
const FIRST_NAME = "Martin";
const LAST_NAME = "Gaens";
echo "My first name is " . FIRST_NAME . " and last name is " . LAST_NAME . ".<br/>";

# 6. In one of your HTML page, write the PHP code to display date.
echo "The current date is " . date("d.m.Y") . ".<br/>";

# 7. Put this variable as a title that is marked as h1 (heading 1) in your HTML document. -->
$title = "PHP is interesting";
echo "<h1>$title</h1><br/>";

# 8. These are the grades for 3 students in the course. Use HTML table into echo and include 3 columns S.n., Name, and grade. Your table should look like the one below:-->
$g1 = 5;
$g2 = 4;
$g3 = 5;
echo "
<style>
    table, th, td {
        border: 2px solid black;
        padding: 3px 7px;
        border-collapse: collapse;
    }
</style>
<table>
    <tr>
        <td><b>S.N</b></td>
        <td><b>Name</b></td>
        <td><b>Grade</b></td>
    </tr>
    <tr>
        <td>1</td>
        <td>Pekka</td>
        <td>$g1</td>
    </tr>
    <tr>
        <td>2</td>
        <td>Johanna</td>
        <td>$g2</td>
    </tr>
    <tr>
        <td>3</td>
        <td>John</td>
        <td>$g3</td>
    </tr>
</table>
";