<?php
//1. Write a php script to display courses as list. Use <li>


$courses = array("PHP", "HTML", "JavaScript", "CMS", "Project");
?>

    <h2>Courses</h2>
    <ul>
		<?php
		foreach ($courses as $course) {
			echo "<li>$course</li>";
		}
		?>
    </ul>

<?php

//2. The unset() function is used to remove element from the array.
//The var_dump() function is used to dump information about a variable.
//array_values() is an inbuilt function that
//returns all the values of an array and not the keys.
//Delete an element from the array below:

$courses1 = array("PHP", "HTML", "JavaScript", "CMS", "Project");
unset($courses1[0]);
// $courses1 without 'PHP'
echo implode(", ", $courses1) . "<br/><br/>";

/*
3. Sort the following array
$courses3=array("PHP", "HTML", "JavaScript", "CMS", "Project");
a) ascending order sort by value
b) ascending order sort by Key
c) descending order sort by Value
d) descending order sort by Key
*/
$courses3 = array("PHP", "HTML", "JavaScript", "CMS", "Project");

asort($courses3);
echo "asc val - " . implode(", ", $courses3) . "<br/>";

ksort($courses3);
echo "asc key - " . implode(", ", $courses3) . "<br/>";

arsort($courses3);
echo "des val - " . implode(", ", $courses3) . "<br/>";

krsort($courses3);
echo "des key - " . implode(", ", $courses3) . "<br/><br/>";


/*
4. Change the following array's all values to upper case.
*/
$courses4 = array("php", "html", "javascript", "cms", "project");
$courses4 = array_map("strtoupper", $courses4);
echo "Upper: " . implode(", ", $courses4) . "<br/><br/>";

// 5. Create an array that holds your favorite colors and print them. (indexed arrays)
$fav_colors = array("red", "green", "blue", "black");
echo "Fav colors: " . implode(", ", $fav_colors) . "<br/><br/>";


// 6. List all your favorite colors and their hexadecimal equivalents. (associative arrays)
$hex_colors = array("red" => "#ff0000", "green" => "#00ff00", "blue" => "#0000ff", "black" => "#000000");
echo "Hex fav colors: " . implode(", ", $hex_colors) . "<br/><br/>";


// 7. Include 12 months in an array named month and print them using loop statement.
$month = array(
	"January",
	"February",
	"March",
	"April",
	"May",
	"June",
	"July",
	"August",
	"September",
	"October",
	"November",
	"December");

foreach ($month as $item) {
    echo $item;
    if ($item != end($month)) echo ", ";
}
echo "<br/><br/>";

/*
8. PHP script to calculate and display average temperature, five lowest and highest temperatures.

Recorded temperatures : 78, 60, 62, 68, 71, 68, 73,
85, 66, 64, 76, 63, 75, 76, 73, 68, 62, 73, 72, 65,
74, 62, 62, 65, 64, 68, 73, 75, 79, 73
Write comments to explain the following code (when asked):
*/
echo "
<hr><h2> Calculation average temperature: </h2>";
$month_temp = "78, 60, 62, 68, 71, 68, 73, 85, 66, 64, 76, 63, 81, 76, 73,
68, 72, 73, 75, 65, 74, 63, 67, 65, 64, 68, 73, 75, 79, 73";
// explode(separator, string) splits the string by the separator into an array, opposite of implode()
$temp_array = explode(',', $month_temp);
$tot_temp = 0;
// count() counts all elements in an array
$temp_array_length = count($temp_array);
foreach ($temp_array as $temp) {
    // I corrected this code, I know it works, but I don't support this kind of type juggling
	$tot_temp += intval($temp);
}
$avg_high_temp = $tot_temp / $temp_array_length;
echo "Average Temperature is : " . $avg_high_temp . "
";
// sort() takes a reference to an indexed array (and doesn't return anything) and it sorts it in ascending order
sort($temp_array);
echo "<br> List of five lowest temperatures :";
for ($i = 0; $i < 5; $i++) {
	echo $temp_array[$i] . ", ";
}
echo "<br>List of five highest temperatures :";
// this loop starts 5 elements from the end of the $temp_array and gradually makes its way to the end of the array.
// Example: if the $temp_array is [10, 20, 30, 40, 50, 60, 70, 80, 90, 100], the length of the array is 10.
// Thus, the $i starts at 10 (length) - 5, thus at 5. It loops until $i is no longer less than the length of the array,
// 10. So it loops through the indices [5, 6, 7, 8, 9], looping through the last 5 elements of the array.
// Probably, a better and more readable alternative to the loop would be using
// implode(", ", array_slice($temp_array, -5));
for ($i = ($temp_array_length - 5); $i < ($temp_array_length); $i++) {
	echo $temp_array[$i] . ", ";
}
?>