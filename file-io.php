<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PHP File IO</title>
</head>
<body>
<?php
//You are required to work as a team and complete the following task during the online session
// we will check your implementation on 16.02.2022 and the team repo must contain the following task
// Each one of you will first do it in your own repo (or branch) and then the final version in the team repo.
// This task is not graded however will have some impact on the final grade
// It also will help you to practice utilizing GitHub in project work

$filename = "text";

# 1. Create/read a text file by using appropriate php functions
# Step 1: check if file exists or not
if (file_exists($filename)) {
	echo "File exists<br/>";
} else {
	die("File doesn't exist.");
}

# Step 2: Open the file using appropriate mode. (each member opens the file in different mode)
$file = fopen($filename, "w+") or die("Unable to open file!");

# Step 3: Use fwrite/fread function to write/read on the file your team name and members name.
fwrite($file, "This is my awesome text!\n");
fseek($file, 0);
echo "File content: \"" . fread($file, filesize($filename)) . "\"<br/><br/>";

# Step 4: Close the file
fclose($file);
?>

<!--2. Uploading files -->
<!--Step 1: Create a simple html form to upload a file.-->
<form action="file-io.php" method="post" enctype="multipart/form-data">
    <input type="file" name="file">
    <input type="submit" value="Submit" name="file-form">
</form>
<?php
if (!(isset($_POST["file-form"]) or isset($_FILES["file"]))) {
	die("No file uploaded yet.");
}

# Step 2: You are required to limit the upload file size to 2 MB.
if ($_FILES["file"]["size"] > 2000000) {
	die("Sorry, your file is too large.");
}

# Step 3: Make sure that users can submit only images.
$check = getimagesize($_FILES["file"]["tmp_name"]);
if ($check !== false) {
	echo "File is an image - " . $check["mime"] . ".<br/>";
} else {
	die("File is not an image.");
}

# Step 4: Upon successful upload, you print a message "File uploaded successfully" and also
# provide the link to the directory where files are uploaded.
if (move_uploaded_file($_FILES["file"]["tmp_name"], "uploads/" . $_FILES["file"]["name"])) {
	echo "File uploaded successfully<br/>";
	echo "<a href='uploads/'>Directory with uploads</a><br/>";
} else {
	die("Sorry, there was an error uploading your file.");
}

?>
</body>
</html>